# vea snacks nutrition label
# API

## selectField

Select field from json object

**Parameters**

-   `attributes` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 
-   `name` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

## renderNutritionRows

Render nutritions with html

**Parameters**

-   `nutritions` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

## renderNutritions

Find nutrition data for products

**Parameters**

-   `accessToken` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 
-   `productApiUrl` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

## getAccessToken

Get access token based on username and password.
Refer to this document <https://docs.mdlzapps.com/#/consumable-service/access-token-api/>

**Parameters**

-   `authorization` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 
    -   `authorization.username` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 
    -   `authorization.password` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 
    -   `authorization.tokenUrl` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 
    -   `authorization.basicAuth` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

**Examples**

```javascript
getAccessToken({
 username: "www.veasnacks.c",
 password: "www.veasnacks.c",
 tokenUrl:  "https://apimanager.mdlzapps.com/token",
 basicAuth: "Basic TTZTN3pzM1FaeUhXUHF3THQxVG5rT1NNbUE0YTpRTUlqRXlTMGNoMUlVMDFqUjVfYjBkX0V3SkFh"
})
```

Returns **[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)** 

## renderProducts

Render products in target element

**Parameters**

-   `targetElement` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** targetElement (optional, default `".products-wrapper"`)
-   `products` **[array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)** products (optional, default `[]`)
-   `authorization` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** authorization (optional, default `{
    username:"www.veasnacks.c",
    password:"www.veasnacks.c",
    tokenUrl:"https://apimanager.mdlzapps.com/token",
    basicAuth:"Basic TTZTN3pzM1FaeUhXUHF3THQxVG5rT1NNbUE0YTpRTUlqRXlTMGNoMUlVMDFqUjVfYjBkX0V3SkFh"
    }`)
-   `productApiUrl` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** productApiUrl (optional, default `"https://apimanager.mdlzapps.com:443/Product/1.0/Product?serviceComponentKey`)

**Examples**

```javascript
var products = [
{
name: "Seed Crackers",
img: "images/SeedCrackersHeader.png",
alt: "Seed Crackers Header",
items: [
{
item: "Greek Hummus with Olive Oil",
slide1: "images/SeedCrackers1-Package.png",
slide2: "images/SeedCrackers1-Description.png",
slide3: "images/Rolling.gif",
slide4: "images/Rolling.gif",
alt1: "Greek Hummus Package",
alt2: "Greek Hummus Description",
alt3: "Greek Hummus Nutrition",
alt4: "Greek Hummus Ingredients",
productId: 44000051242
},
{
item: "Mexican Garden Herbs",
slide1: "images/SeedCrackers2-Package.png",
slide2: "images/SeedCrackers2-Description.png",
slide3: "images/Rolling.gif",
slide4: "images/Rolling.gif",
alt1: "Mexican Garden Package",
alt2: "Mexican Garden Description",
alt3: "Mexican Garden Nutrition",
alt4: "Mexican Garden Ingredients",
productId: 44000051228
}
]
}

// render products to element's className is "product-wrapper"
renderProducts(
".products-wrapper",
products,
{
username: "www.veasnacks.c",
password: "www.veasnacks.c",
tokenUrl: "https://apimanager.mdlzapps.com/token",
basicAuth:
"Basic TTZTN3pzM1FaeUhXUHF3THQxVG5rT1NNbUE0YTpRTUlqRXlTMGNoMUlVMDFqUjVfYjBkX0V3SkFh"
},
"https://apimanager.mdlzapps.com:443/Product/1.0/Product?serviceComponentKey=47D35E52-01D2-4FE2-9C0C-8266090AF1AD&productId="
);
```

## sliderNavigation

Make the slider for products using slick library

**Parameters**

-   `sliderElement` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** navAnchor (optional, default `".nav-anchors"`)
-   `navAnchor`   (optional, default `".nav-anchors"`)
